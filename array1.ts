const names: String[] = [];
names.push("Dylan");
names.push("Worawit");

console.log(names[0]);
console.log(names[1]);
console.log(names.length);

for (let i = 0; i < names.length; i++){
    console.log(names[i]);
}

for (let inx in names) {
    console.log(names[inx]);
}
names.forEach(function(name){
    console.log(name);
});

